class ProfilesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]
  before_action :set_profile, only: [:edit, :update, :show]
  before_action :check_current_user, only: [:edit, :update]

  def edit
  end

  def update
    if @profile.update(profile_params)
      @multipliers = @profile.sort_multipliers( profile_params )
      @profile.update({
        m1: @multipliers[0],
        m2: @multipliers[1],
        m3: @multipliers[2],
        m4: @multipliers[3]
      })
      redirect_to edit_profile_path(@profile.id), notice: 'Your info was successfully edited'
    else
      render :edit
    end
  end

  def show
    @publicImages = Image.public_ones_by_profile(@profile.id)
    # abort @publicImages[0].inspect
    @donations = @profile.set_donations
  end

  private

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def check_current_user
    if current_user.id != @profile.id
      redirect_to root_path
    end
  end

  def profile_params
    params.require(:profile).permit(:fullname, :bio, :occupation, :amount, :currency, :m1, :m2, :m3, :m4, :avatar)
  end
end
