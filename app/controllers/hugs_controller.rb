class HugsController < ApplicationController
  def new
    @profile = Profile.find(params[:profile_id])
    @hug = Hug.new
  end

  def create
    @profile = Profile.find(params[:profile_id])
    @hug = @profile.hugs.build({duration: params[:hug][:duration]})
    if @hug.save
      @hug.build_donation({
        occupation: params[:hug][:occupation],
        message: params[:hug][:message],
        contributor: params[:hug][:contributor],
      }).save
      redirect_to donationsmanager_thanks_path(@profile), notice: 'Thank you for your donation!'
    else
      render :new
    end
  end
end
