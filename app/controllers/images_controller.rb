class ImagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_profile, only: [:index, :new, :create]
  before_action :set_image, only: [:edit, :update, :destroy]

  before_action :check_current_user, only: [:index, :new]

  def index
    @images = @profile.images
  end

  def new
    @image = Image.new
  end

  def create
    @image = @profile.images.build(image_params)
    if @image.save
      redirect_to profile_images_path(@profile), notice: 'image was successfully created.'
    else
      render :new
    end
  end

  def edit

  end

  def update
    @profile = @image.profile.id
    if @image.update(image_params)
      redirect_to profile_images_path(@profile), notice: 'Your info was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile = @image.profile.id
    @image.destroy
    redirect_to profile_images_path(@profile), notice: 'Info was successfully destroyed.'
  end

  private

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_image
    @image = Image.find(params[:id])
  end

  def check_current_user
    if current_user.id != @profile.id
      redirect_to root_path
    end
  end

  def image_params
    params.require(:image).permit(:subtitle, :public, :photo)
  end
end
