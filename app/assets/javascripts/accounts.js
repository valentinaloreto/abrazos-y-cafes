
document.addEventListener("turbolinks:load", function() {

  var bank = document.getElementById('bank');
  var paypal = document.getElementById('paypal');

  var banksections = document.querySelectorAll('.banksection');
  var paypalsections = document.querySelectorAll('.paypalsection');


  function revealme () {
    switch (this.id) {
      case 'bank':
        banksections.forEach(function(element){ element.classList.remove('d-none'); });
        paypalsections.forEach(function(element){ element.classList.add('d-none'); });
        break;
      case 'paypal':
        paypalsections.forEach(function(element){ element.classList.remove('d-none'); });
        banksections.forEach(function(element){ element.classList.add('d-none'); });
        break;
    }
  }

  bank.addEventListener('click',revealme);
  paypal.addEventListener('click',revealme);

});


document.addEventListener("turbolinks:load", function() {

  var bankEdit = document.getElementById('bankedit');
  var paypalEdit = document.getElementById('paypaledit');

  var banksections = document.querySelectorAll('.banksection');
  var paypalsections = document.querySelectorAll('.paypalsection');

  function revealmeEdit () {
    switch (this.id) {
      case 'bankedit':
        banksections.forEach(function(element){ element.classList.remove('d-none'); });
        paypalsections.forEach(function(element){ element.classList.add('d-none'); });
        break;
      case 'paypaledit':
        paypalsections.forEach(function(element){ element.classList.remove('d-none'); });
        banksections.forEach(function(element){ element.classList.add('d-none'); });
        break;
    }
  }

  bankEdit.addEventListener('click',revealmeEdit);
  paypalEdit.addEventListener('click',revealmeEdit);

});
