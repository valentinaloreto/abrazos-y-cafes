// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/


document.addEventListener("turbolinks:load", function() {

  var checkbox = document.querySelector('.box-input');
  var pinkbox = document.querySelector('.styled-checkbox');

  if ( checkbox.checked == true ) {
    pinkbox.classList.add('pinkmeup1');
  }

  pinkbox.addEventListener('click', function(){
    if ( checkbox.checked == true ) {
      pinkbox.classList.add('pinkmeup1');
    } else  {
      pinkbox.classList.remove('pinkmeup1');
    }
  });

});
