
document.addEventListener("turbolinks:load", function() {

// ****** SPLIT FORM variables******

  var testme = document.querySelector('#testme');
  var hugsection =document.querySelector('.hug-section');
  var formsection =document.querySelector('.form-section');
  var submitcontainer = document.querySelector('.hugs-submit-container');
  var hideme = document.querySelector('.hide-me');

// ****** HUG GIVING variables*******

  var hugbutton = document.querySelector('.hug-touch');
  var seconddisplayer = document.querySelectorAll('.seconds');
  var milisecondsdisplayer = document.querySelectorAll('.miliseconds');
  var inputseconds = document.querySelector('.inputseconds');
  var minicircles = document.querySelectorAll('.mini-circles');

//********** SPLIT FORM EVENT LISTENER ************

  testme.addEventListener('click', function(){
    hugsection.classList.add('d-none');
    formsection.classList.remove('d-none');
    submitcontainer.classList.remove('d-none');
    testme.classList.add('d-none');
    hideme.classList.add('d-none');
  });

// ****************HUG GIVING FUNCTIONS************

  function activateHug(event) {

      event.preventDefault();
      initialwidthheight = 50;
      hugbutton.style.backgroundColor = ' white';
      stophug = false;
      seconds = 0;
      miliseconds = 0;
      seconddisplayer.forEach(function(element){
        element.innerHTML = 0 + ".";
        inputseconds.value = 0;
      });

      handletimer = setInterval(function(){
        if (!stophug) {
          if (seconds < 30) {
            seconds += 1;
            seconddisplayer.forEach(function(element){
              element.innerHTML = seconds + ".";
              inputseconds.value = seconds;
            });
          }
        }
      }, 1000);

      handlemiliseconds = setInterval(function(){
        if (!stophug) {
          if (seconds < 30 ) {
            miliseconds += 1;
            milisecondsdisplayer.forEach(function(element){
              if (miliseconds < 10) {
                element.innerHTML = "0" + miliseconds;
              } else if (miliseconds < 60) {
                element.innerHTML = miliseconds;
              } else {
                miliseconds = 0;
              }
            });
          } else {
            milisecondsdisplayer.forEach(function(element){
              element.innerHTML = "00";
            });
          }
        }
      }, 1);

      handlefiller = setInterval(function(){
        if (!stophug) {
          initialwidthheight += 1.5;
          minicircles.forEach(function(circle){
            circle.style.height = initialwidthheight + "px";
            circle.style.width =  initialwidthheight + "px";
          });
        }
        if (seconds > 29) { hugbutton.style.backgroundColor = '#EE4380'; }
      }, 100);
  }

  function deactivateHug() {
    stophug = true;
    clearInterval(handletimer);
    clearInterval(handlefiller);
    clearInterval(handlemiliseconds);
  }

//********** HUG EVENT LISTENERS ************

  hugbutton.addEventListener('mousedown',activateHug);
  hugbutton.addEventListener('mouseup', deactivateHug);

  hugbutton.addEventListener('touchstart',activateHug);
  hugbutton.addEventListener('touchend',deactivateHug);

});
