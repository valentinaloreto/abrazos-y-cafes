

document.addEventListener("turbolinks:load", function() {

  var radiobutton1 = document.querySelector('#radiobutton1');
  var radiobutton2 = document.querySelector('#radiobutton2');
  var radiobutton3 = document.querySelector('#radiobutton3');
  var radiobutton4 = document.querySelector('#radiobutton4');

  function radioButtonClicked() {
    var price = parseFloat( document.querySelector('#amount').innerHTML );
    var total = document.querySelector('#total');
    totalCoffees = this.value * price;

    if ( !isNaN(totalCoffees) ) { total.innerHTML =  totalCoffees; }

    var allradiocircles = document.querySelectorAll('.radio-circle');
    allradiocircles.forEach(function(element){
      element.classList.remove('pinkmeup');
    })

    var radiocircle = this.parentNode;
    radiocircle.classList.add('pinkmeup');
  }

  radiobutton1.addEventListener('click', radioButtonClicked);
  radiobutton2.addEventListener('click', radioButtonClicked);
  radiobutton3.addEventListener('click', radioButtonClicked);
  radiobutton4.addEventListener('click', radioButtonClicked);
});
