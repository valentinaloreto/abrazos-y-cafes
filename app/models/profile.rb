class Profile < ApplicationRecord
  belongs_to :user
  has_one_attached :avatar
  has_many :accounts
  has_many :images
  has_many :beverages
  has_many :hugs


  def public_images
    self.images.select { |i| i.public == true }
  end

  def sort_multipliers(params)
    # abort params.inspect
    p = params.select{ |k,v| ["m1", "m2", "m3", "m4"].include? k }
    a = p.to_unsafe_h.map{ |k, v| v.to_i }.sort!
    # abort a.inspect
  end

  def set_donations
    @donations = [];
    @donations << self.beverages
    @donations << self.hugs
    @donations.flatten!
    @donations = @donations.sort_by &:created_at
  end

end
