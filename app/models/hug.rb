class Hug < ApplicationRecord
  belongs_to :profile
  has_one :donation, as: :donable
end
