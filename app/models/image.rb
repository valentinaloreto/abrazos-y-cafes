class Image < ApplicationRecord
  belongs_to :profile
  has_one_attached :photo

  scope :public_ones, -> { where(public: true) }

  scope :public_ones_by_profile, -> (profileID) {
    public_ones.where("profile_id = ?", profileID)
  }

end
