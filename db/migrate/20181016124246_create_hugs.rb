class CreateHugs < ActiveRecord::Migration[5.2]
  def change
    create_table :hugs do |t|
      t.references :profile, foreign_key: true
      t.string :duration

      t.timestamps
    end
  end
end
